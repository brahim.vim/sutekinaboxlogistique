<?php

namespace App\Repository;

use App\Entity\Box;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Box|null find($id, $lockMode = null, $lockVersion = null)
 * @method Box|null findOneBy(array $criteria, array $orderBy = null)
 * @method Box[]    findAll()
 * @method Box[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BoxRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Box::class);
    }

    public function getBoxValide($service)
    {
        $req = $this->createQueryBuilder('b');
        if ($service === 'ROLES_FOURNISSEUR') {
            $req->where('b.statutMarketing = 1');
        } else if ($service === 'ROLES_EMBALLAGE'){
            $req->where('b.statutMarketing = 1');
            $req->andWhere('b.statutFournisseur = 1');
        } else if ($service === 'ROLE_LIVRAISON'){
            $req->where('b.statutMarketing = 1');
            $req->andWhere('b.statutFournisseur = 1');
            $req->andWhere('b.statutEmballage = 1');
        }

        return $req->orderBy('b.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function valideBox()
    {

    }

//    /**
//     * @return Box[] Returns an array of Box objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Box
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
