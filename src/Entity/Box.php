<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BoxRepository")
 */
class Box
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $nom;


    /**
     * @ORM\Column(type="boolean")
     */
    private $statutMarketing;

    /**
     * @ORM\Column(type="boolean")
     */
    private $statutFournisseur;

    /**
     * @ORM\Column(type="boolean")
     */
    private $statutAchat;

    /**
     * @ORM\Column(type="boolean")
     */
    private $statutEmballage;

    /**
     * @ORM\Column(type="boolean")
     */
    private $statutLivraison;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Produits")
     */
    private $produits;


    public function __construct()
    {
        $this->statutAchat          = false;
        $this->statutFournisseur    = false;
        $this->statutAchat          = false;
        $this->statutEmballage      = false;
        $this->statutLivraison      = false;
        $this->statutMarketing      = 1; // Il y a que le service marketing qui peut créer des boxes donc par défaut on le met à 1
        $this->produits             = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * @param mixed $statut
     */
    public function setStatut($statut): void
    {
        $this->statut = $statut;
    }

    /**
     * @return mixed
     */
    public function getStatutMarketing()
    {
        return $this->statutMarketing;
    }

    /**
     * @param mixed $statutMarketing
     */
    public function setStatutMarketing($statutMarketing)
    {
        $this->statutMarketing = $statutMarketing;
    }

    /**
     * @return mixed
     */
    public function getStatutFournisseur()
    {
        return $this->statutFournisseur;
    }

    /**
     * @param mixed $statutFournisseur
     */
    public function setStatutFournisseur($statutFournisseur)
    {
        $this->statutFournisseur = $statutFournisseur;
    }

    /**
     * @return mixed
     */
    public function getStatutAchat()
    {
        return $this->statutAchat;
    }

    /**
     * @param mixed $statutAchat
     */
    public function setStatutAchat($statutAchat)
    {
        $this->statutAchat = $statutAchat;
    }

    /**
     * @return mixed
     */
    public function getStatutEmballage()
    {
        return $this->statutEmballage;
    }

    /**
     * @param mixed $statutEmballage
     */
    public function setStatutEmballage($statutEmballage)
    {
        $this->statutEmballage = $statutEmballage;
    }

    /**
     * @return mixed
     */
    public function getStatutLivraison()
    {
        return $this->statutLivraison;
    }

    /**
     * @param mixed $statutLivraison
     */
    public function setStatutLivraison($statutLivraison)
    {
        $this->statutLivraison = $statutLivraison;
    }

    /**
     * @return Collection|Produits[]
     */
    public function getProduits(): Collection
    {
        return $this->produits;
    }

    public function addProduit(Produits $produit): self
    {
        if (!$this->produits->contains($produit)) {
            $this->produits[] = $produit;
        }

        return $this;
    }

    public function removeProduit(Produits $produit): self
    {
        if ($this->produits->contains($produit)) {
            $this->produits->removeElement($produit);
        }

        return $this;
    }
    
}
