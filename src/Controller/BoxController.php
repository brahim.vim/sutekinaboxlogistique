<?php
/**
 * Created by PhpStorm.
 * User: Etudiant0
 * Date: 10/07/2018
 * Time: 14:48
 */

namespace App\Controller;



use App\Entity\Box;
use App\Form\BoxType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BoxController extends Controller
{
    /**
     * @Route("/creer-box", name="creer_box")
     */
    public function creation(Request $request)
    {
        $box = new Box();

        $form = $this->createForm(BoxType::class, $box);

        // Traitement du form
        $requeteForm = $form->handleRequest($request);

        // Vérife si formulaire valide et soumit
        if ($requeteForm->isSubmitted() && $requeteForm->isValid()){
            $box = $requeteForm->getData(); // Récupe des données

            // Insertion en bdd
            $em = $this->getDoctrine()->getManager();
            $em->persist($box);
            $em->flush();

            $this->addFlash('success', 'Votre box a bien été créée');
            return $this->redirectToRoute('home');
        }

        return $this->render('creation_box.html.twig', [
            'form' => $form->createView()
        ]);
    }
}