<?php
/**
 * Created by PhpStorm.
 * User: Etudiant0
 * Date: 11/07/2018
 * Time: 14:16
 */

namespace App\Controller;


use App\Entity\Box;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Routing\Annotation\Route;

class FournisseurController extends Controller
{

    /**
     * @Route("/index-fournisseur", name="index_fournisseur")
     */
    public function index()
    {
        $box = $this
            ->getDoctrine()
            ->getRepository(Box::class)
            ->getBoxValide('ROLES_FOURNISSEUR');

        return $this->render('index.fournisseur.html.twig', [
            'liste_boxe'    => $box,
            'service'       => $this->getUser()->getRoles()[0]
        ]);
    }

    /**
     * @Route("/valider_box/{id}/{service}", name="validerbox")
     * @param $id
     * @param $service
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function valideBox($id, $service)
    {
        if ($id) {
            $box = $this
                ->getDoctrine()
                ->getRepository(Box::class)
                ->valideBox($id, $service);

            $this->addFlash('success', 'La box a été validée.');

            return $this->render('index.fournisseur.html.twig', [
                'liste_boxe' => $box
            ]);
        } else {
            throw new Exception("Besoin de l'id pour valider la box");
        }
    }

}