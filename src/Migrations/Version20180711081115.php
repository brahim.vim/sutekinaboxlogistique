<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180711081115 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE box ADD statut_fournisseur TINYINT(1) NOT NULL, ADD statut_achat TINYINT(1) NOT NULL, ADD statut_emballage TINYINT(1) NOT NULL, ADD statut_livraison TINYINT(1) NOT NULL, DROP statut, CHANGE valide statut_marketing TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE box ADD valide TINYINT(1) NOT NULL, ADD statut VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, DROP statut_marketing, DROP statut_fournisseur, DROP statut_achat, DROP statut_emballage, DROP statut_livraison');
    }
}
