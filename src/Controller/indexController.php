<?php
/**
 * Created by PhpStorm.
 * User: Etudiant0
 * Date: 09/07/2018
 * Time: 14:36
 */

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class indexController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $route = $this->getUser() == null ? '' : $this->getUser()->getRoles()[0];

        switch ($route){
            case 'ROLE_ADMIN':
                return $this->render('admin.html.twig');
                break;
            case 'ROLE_MARKETING':
                return $this->render('index.marketing.html.twig');
                break;
            case 'ROLE_FOURNISSEUR':
                return $this->redirectToRoute('index_fournisseur');
                break;
            case 'ROLE_ACHAT':
                return $this->render('index.achat.html.twig');
                break;
            case 'ROLE_EMBALLAGE':
                return $this->render('index.emballage.html.twig');
                break;
            case 'ROLE_LIVRAISON':
                return $this->render('index.livraison.html.twig');
                break;

            default:
                return $this->render('index/index.html.twig');
                break;
        }

    }
}