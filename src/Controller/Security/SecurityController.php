<?php
/**
 * Created by PhpStorm.
 * User: Etudiant0
 * Date: 29/06/2018
 * Time: 14:55
 */

namespace App\Controller\Security;



use App\Form\LoginType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends Controller
{
    /**
     * Connexion d un user
     * @Route("/connexion", name="security_login")
     * @param Request $request
     * @param AuthenticationUtils $authenticationUtils
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils)
    {
        // Stock l'entité user dans $this->getUser()
        // Si user authentifié redirige vers page d'accueil

        if ($this->getUser() ){
            return $this->redirectToRoute('home');
        }

        //Récupération du form
        $form = $this->createForm(LoginType::class, [
            'email' => $authenticationUtils->getLastUsername()
        ]);

        // Récupération du message d'erreur s'il y en a un
        $error = $authenticationUtils->getLastAuthenticationError();

        // Dernier email saisir par l'utilisateur
        $lastEmail = $authenticationUtils->getLastUsername();

        // Transmission à la vue
        return $this->render('security/login.html.twig', [
           'form' => $form->createView(),
           'error' => $error
        ]);
    }

    /**
     * @Route("/deconnexion", name="security_logout")
     */
    public function logout()
    {

    }

    /**
     * Définir votre logique mot de passe oublié
     * et Réinitialisation du mot de passe
     */
}